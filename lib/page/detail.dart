import 'dart:async';

import 'package:flutter/material.dart';
import 'package:producer_reference/widget/appbar.dart';
import 'package:producer_reference/widget/responsive.dart';

class DetailsPage extends StatefulWidget {
  final int? index;
  DetailsPage({Key? key, this.index}) : super(key: key);

  @override
  DetailsPageState createState() => DetailsPageState();
}

class DetailsPageState extends State<DetailsPage> {
  final PageController? controller = PageController(viewportFraction: 0.8);
  int? pageIndex = 0;

  Future<bool> initializeController() {
    Completer<bool> completer = new Completer<bool>();

    /// Callback called after widget has been fully built
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      completer.complete(true);
    });

    return completer.future;
  }

  @override
  void initState() {
    super.initState();
    controller?.addListener(() {
      if (controller?.page != pageIndex)
        setState(() => pageIndex = controller?.page?.round());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PPAppBar(
        leading: Builder(
          builder: (context) => IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Détails",
              style: TextStyle(
                color: Colors.black,
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
      body: Padding(
        padding: Responsive.of(context).appBarPaddingBuilder(context),
        child: Material(
          borderRadius: BorderRadius.circular(10.0),
          elevation: 5.0,
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: ListView(
              children: [
                SizedBox(
                  height: 40.0,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.04),
                  child: Text(
                    "Qui suis-je ?",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 30.0,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.04),
                  child: Text(
                    "Présentation plus ou moins brève le producteur fera comme bon lui semble... Présentation plus ou moins brève le producteur fera comme bon lui semble... Présentation plus ou moins brève le producteur fera comme bon lui semble... Présentation plus ou moins brève le producteur fera comme bon lui semble...",
                    style: TextStyle(
                      color: Colors.black54,
                      fontSize: 18.0,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 40.0),
                  child: AspectRatio(
                    aspectRatio: 4 / 2,
                    child: PageView.builder(
                      itemCount: 5,
                      scrollDirection: Axis.horizontal,
                      controller: controller,
                      itemBuilder: (BuildContext context, index) {
                        if (index == 0)
                          return Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20.0),
                            child: Hero(
                              tag: "détails${widget.index}",
                              child: Container(
                                clipBehavior: Clip.antiAlias,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                child: Image.network(
                                  "https://media.istockphoto.com/photos/traditional-french-farmhouse-picture-id95732671?s=612x612",
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          );
                        else
                          return Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20.0),
                            child: Container(
                              clipBehavior: Clip.antiAlias,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Image.network(
                                "https://media.istockphoto.com/photos/traditional-french-farmhouse-picture-id95732671?s=612x612",
                                fit: BoxFit.fill,
                              ),
                            ),
                          );
                      },
                    ),
                  ),
                ),
                FutureBuilder(
                  future: initializeController(),
                  builder:
                      (BuildContext context, AsyncSnapshot<void> snapshot) {
                    if (!snapshot.hasData) return SizedBox();
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        IconButton(
                          tooltip: "Page précédante",
                          onPressed: () => controller?.previousPage(
                            duration: Duration(milliseconds: 600),
                            curve: Curves.easeInOutExpo,
                          ),
                          icon: Icon(
                            Icons.arrow_left,
                            color: Colors.grey,
                          ),
                        ),
                        for (int i = 0; i < 5; i++)
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: CircleAvatar(
                              radius: 3.0,
                              backgroundColor: pageIndex == i
                                  ? Colors.grey
                                  : Colors.grey.shade300,
                            ),
                          ),
                        IconButton(
                          tooltip: "Page suivante",
                          onPressed: () => controller?.nextPage(
                            duration: Duration(milliseconds: 600),
                            curve: Curves.easeInOutExpo,
                          ),
                          icon: Icon(
                            Icons.arrow_right,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    );
                  },
                ),
                SizedBox(
                  height: 30.0,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.04),
                  child: Text(
                    "Comment je produis ?",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 30.0,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.04),
                  child: Text(
                    "Présentation plus ou moins brève le producteur fera comme bon lui semble... Présentation plus ou moins brève le producteur fera comme bon lui semble... Présentation plus ou moins brève le producteur fera comme bon lui semble... Présentation plus ou moins brève le producteur fera comme bon lui semble...",
                    style: TextStyle(
                      color: Colors.black54,
                      fontSize: 18.0,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 40.0),
                  child: AspectRatio(
                    aspectRatio: 4 / 2,
                    child: PageView.builder(
                      itemCount: 5,
                      scrollDirection: Axis.horizontal,
                      controller: controller,
                      itemBuilder: (BuildContext context, index) {
                        if (index == 0)
                          return Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20.0),
                            child: Hero(
                              tag: "détails${widget.index}",
                              child: Container(
                                clipBehavior: Clip.antiAlias,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                child: Image.network(
                                  "https://media.istockphoto.com/photos/traditional-french-farmhouse-picture-id95732671?s=612x612",
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          );
                        else
                          return Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20.0),
                            child: Container(
                              clipBehavior: Clip.antiAlias,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Image.network(
                                "https://media.istockphoto.com/photos/traditional-french-farmhouse-picture-id95732671?s=612x612",
                                fit: BoxFit.fill,
                              ),
                            ),
                          );
                      },
                    ),
                  ),
                ),
                FutureBuilder(
                  future: initializeController(),
                  builder:
                      (BuildContext context, AsyncSnapshot<void> snapshot) {
                    if (!snapshot.hasData) return SizedBox();
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        IconButton(
                          tooltip: "Page précédante",
                          onPressed: () => controller?.previousPage(
                            duration: Duration(milliseconds: 600),
                            curve: Curves.easeInOutExpo,
                          ),
                          icon: Icon(
                            Icons.arrow_left,
                            color: Colors.grey,
                          ),
                        ),
                        for (int i = 0; i < 5; i++)
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: CircleAvatar(
                              radius: 3.0,
                              backgroundColor: pageIndex == i
                                  ? Colors.grey
                                  : Colors.grey.shade300,
                            ),
                          ),
                        IconButton(
                          tooltip: "Page suivante",
                          onPressed: () => controller?.nextPage(
                            duration: Duration(milliseconds: 600),
                            curve: Curves.easeInOutExpo,
                          ),
                          icon: Icon(
                            Icons.arrow_right,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
