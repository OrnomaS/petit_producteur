import 'package:flutter/material.dart';
import 'package:producer_reference/widget/appbar.dart';
import 'package:producer_reference/widget/card.dart';
import 'package:producer_reference/widget/chip.dart';
import 'package:producer_reference/widget/responsive.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(),
      appBar: PPAppBar(
        leading: Builder(
          builder: (context) => IconButton(
            icon: Icon(Icons.dehaze),
            onPressed: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        child: PPSearchBar(),
      ),
      body: Padding(
        padding: Responsive.of(context).appBarPaddingBuilder(context),
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(vertical: 7.5),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Wrap(
                  spacing: 10.0,
                  runSpacing: 7.5,
                  children: [
                    CityChip(city: "Prouvy"),
                    CityChip(city: "Raismes"),
                    CityChip(city: "Prouvy"),
                    CityChip(city: "Raismes"),
                    CityChip(city: "Prouvy"),
                    CityChip(city: "Raismes"),
                    CityChip(city: "Prouvy"),
                    CityChip(city: "Raismes"),
                    CityChip(city: "Prouvy"),
                    CityChip(city: "Raismes"),
                  ],
                ),
              ),
            ),
            Expanded(
              child: GridView.builder(
                scrollDirection: Axis.vertical,
                itemCount: 5,
                itemBuilder: (context, index) => PPCard(index: index),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount:
                      Responsive.of(context).crossAxisCount(context),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {},
        tooltip: 'Ajouter un producteur',
        child: Icon(Icons.add),
      ),
    );
  }
}
