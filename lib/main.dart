import 'package:flutter/material.dart';
import 'package:producer_reference/page/home.dart';
import 'package:producer_reference/widget/responsive.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Responsive(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Petit Producteur',
        theme: ThemeData(
          primarySwatch: Colors.green,
          chipTheme: ChipThemeData(
            backgroundColor: Colors.grey.shade300,
            disabledColor: Colors.grey,
            selectedColor: Colors.green.shade300,
            secondarySelectedColor: Colors.green.shade400,
            padding: EdgeInsets.all(2.0),
            labelStyle: TextStyle(color: Colors.black),
            secondaryLabelStyle: TextStyle(color: Colors.white),
            brightness: Brightness.light,
          ),
        ),
        home: HomePage(),
      ),
    );
  }
}