import 'package:flutter/material.dart';
import 'package:producer_reference/page/detail.dart';

class PPCard extends StatelessWidget {
  final int? index;
  PPCard({this.index});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5.0,
      clipBehavior: Clip.antiAlias,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListTile(
            title: Text('Eric Dupon'),
            subtitle: Text(
              'Je suis producteur de de bétrave bio et éleveur de poulets.',
              style: TextStyle(color: Colors.black.withOpacity(0.6)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 16.0,
              bottom: 16.0,
              right: 16.0,
              left: 16.0,
            ),
            child: Text(
              "Méthodes de productions, d'élevages en un cours résumé.",
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Colors.black.withOpacity(0.6),
              ),
            ),
          ),
          ButtonBar(
            alignment: MainAxisAlignment.start,
            children: [
              TextButton(
                onPressed: () {},
                child: Padding(
                  padding: EdgeInsets.all(5.0),
                  child: Text(
                    'Contacter',
                    style: TextStyle(
                      color: Colors.green,
                    ),
                  ),
                ),
              ),
              TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    CustomPageRoute(
                      builder: (_) => DetailsPage(index: index),
                    ),
                  );
                },
                child: Padding(
                  padding: EdgeInsets.all(5.0),
                  child: Text(
                    'Détails',
                    style: TextStyle(
                      color: Colors.green,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: Hero(
              tag: "détails$index",
              child: Image.network(
                "https://media.istockphoto.com/photos/traditional-french-farmhouse-picture-id95732671?s=612x612",
                fit: BoxFit.fitWidth,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CustomPageRoute extends MaterialPageRoute {
  @override
  Duration get transitionDuration => const Duration(milliseconds: 1000);

  CustomPageRoute({builder}) : super(builder: builder);
}
