import 'package:flutter/material.dart';

class CityChip extends StatefulWidget {
  final String? city;
  CityChip({Key? key, this.city}) : super(key: key);

  @override
  _CityChipState createState() => _CityChipState();
}

class _CityChipState extends State<CityChip> {
  bool _isSelected = false;

  @override
  Widget build(BuildContext context) {
    return ChoiceChip(
      label: Text(
        '${widget.city}',
      ),
      selected: _isSelected,
      onSelected: (bool selected) => setState(() => _isSelected = selected),
    );
  }
}
