import 'package:flutter/material.dart';
import 'package:producer_reference/widget/responsive.dart';

class PPAppBar extends PreferredSize {
  final Builder leading;
  PPAppBar({
    required Widget child,
    required this.leading,
  }) : super(
          child: child,
          preferredSize: Size.fromHeight(kToolbarHeight),
        );

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: Responsive.of(context).appBarPaddingBuilder(context),
      child: Material(
        borderRadius: BorderRadius.circular(10.0),
        elevation: 5.0,
        child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: 15.0,
          ),
          alignment: Alignment.centerLeft,
          child: Row(
            children: [
              leading,
              SizedBox(
                width: 15.0,
              ),
              Expanded(
                child: child,
              ),
            ],
          ),
          height: preferredSize.height,
        ),
      ),
    );
  }
}

class PPSearchBar extends StatelessWidget {
  const PPSearchBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: TextField(
            style: TextStyle(
              fontSize: 20.0,
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: "Rechercher par ville...",
              hintStyle: TextStyle(
                fontSize: 20.0,
              ),
            ),
          ),
        ),
        SizedBox(
          width: 15.0,
        ),
        Icon(
          Icons.grass,
          color: Colors.green,
          size: 26.0,
        ),
      ],
    );
  }
}
