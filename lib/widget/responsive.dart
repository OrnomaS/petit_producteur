import 'package:flutter/material.dart';

class Responsive extends InheritedWidget {
  Responsive({
    child,
  }) : super(child: child);

  @override
  bool updateShouldNotify(Responsive oldWidget) => this != oldWidget;

  static Responsive of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<Responsive>() as Responsive;

  EdgeInsetsGeometry appBarPaddingBuilder(BuildContext context) =>
      MediaQuery.of(context).size.width < 1080
          ? EdgeInsets.all(5.0)
          : EdgeInsets.symmetric(
              vertical: 5.0,
              horizontal: (MediaQuery.of(context).size.width * 2) / 10,
            );

  int crossAxisCount(BuildContext context) =>
      MediaQuery.of(context).size.width < 1080 ? 1 : 2;
}
